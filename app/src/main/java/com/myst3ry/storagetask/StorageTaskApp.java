package com.myst3ry.storagetask;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.myst3ry.storagetask.data.NodeDatabase;

public final class StorageTaskApp extends Application {

    private NodeDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        initDb();
    }

    private void initDb() {
        db = Room.databaseBuilder(this, NodeDatabase.class, "node_db").build();
    }

    public static StorageTaskApp getApp(final Context context) {
        return ((StorageTaskApp)context.getApplicationContext());
    }

    public NodeDatabase getDb() {
        return db;
    }
}
