package com.myst3ry.storagetask.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import com.myst3ry.storagetask.data.model.NodeRelation;

@Dao
public interface NodeRelationDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertNodeRelation(final NodeRelation relation);

    @Delete
    void deleteNodeRelation(final NodeRelation relation);
}
