package com.myst3ry.storagetask.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "node")
public final class Node {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private final int value;

    public Node(final int value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public void setId(final long id) {
        this.id = id;
    }
}
