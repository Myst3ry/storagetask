package com.myst3ry.storagetask.data.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.ArrayList;
import java.util.List;

public final class NodeWithRelations {

    @Embedded
    private Node node;

    @Relation(parentColumn = "id", entityColumn = "parent_id")
    private List<NodeRelation> children;

    @Relation(parentColumn = "id", entityColumn = "child_id")
    private List<NodeRelation> parents;

    public List<NodeWithRelations> getParentsFrom(final List<NodeWithRelations> nodes) {
        final List<NodeWithRelations> parentsList = new ArrayList<>();
        for (NodeWithRelations node : nodes) {
            for (NodeRelation relation : parents) {
                if (relation.getParentId() == node.node.getId()) {
                    parentsList.add(node);
                }
            }
        }
        return parentsList;
    }

    public List<NodeWithRelations> getChildrenFrom(final List<NodeWithRelations> nodes) {
        final List<NodeWithRelations> childrenList = new ArrayList<>();
        for (NodeWithRelations node : nodes) {
            for (NodeRelation relation : children) {
                if (relation.getChildId() == node.node.getId()) {
                    childrenList.add(node);
                }
            }
        }
        return childrenList;
    }

    public boolean isParentOf(final long nodeId) {
        for (NodeRelation relation : children) {
            if (relation.getChildId() == nodeId) {
                return true;
            }
        }
        return false;
    }

    public boolean isChildOf(final long nodeId) {
        for (NodeRelation relation : parents) {
            if (relation.getParentId() == nodeId) {
                return true;
            }
        }
        return false;
    }

    public void setNode(final Node node) {
        this.node = node;
    }

    public void setChildren(final List<NodeRelation> children) {
        this.children = children;
    }

    public void setParents(final List<NodeRelation> parents) {
        this.parents = parents;
    }

    public Node getNode() {
        return node;
    }

    public List<NodeRelation> getChildren() {
        return children;
    }

    public List<NodeRelation> getParents() {
        return parents;
    }
}
