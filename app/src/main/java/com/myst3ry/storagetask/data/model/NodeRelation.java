package com.myst3ry.storagetask.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

@Entity(tableName = "node_relation",
        primaryKeys = {"parent_id", "child_id"},
        foreignKeys = {
                @ForeignKey(entity = Node.class,
                        parentColumns = "id",
                        childColumns = "parent_id",
                        onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Node.class,
                        parentColumns = "id",
                        childColumns = "child_id",
                        onDelete = ForeignKey.CASCADE)
        },
        indices = {
                @Index(value = "parent_id"),
                @Index(value = "child_id")
        })
public final class NodeRelation {

    @ColumnInfo(name = "parent_id")
    private final long parentId;

    @ColumnInfo(name = "child_id")
    private final long childId;

    public NodeRelation(final long parentId, final long childId) {
        this.parentId = parentId;
        this.childId = childId;
    }

    public long getParentId() {
        return parentId;
    }

    public long getChildId() {
        return childId;
    }
}