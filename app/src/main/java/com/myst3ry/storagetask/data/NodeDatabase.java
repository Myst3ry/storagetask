package com.myst3ry.storagetask.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.myst3ry.storagetask.data.dao.NodeDAO;
import com.myst3ry.storagetask.data.dao.NodeRelationDAO;
import com.myst3ry.storagetask.data.model.Node;
import com.myst3ry.storagetask.data.model.NodeRelation;

@Database(entities = {Node.class, NodeRelation.class}, version = 5)
public abstract class NodeDatabase extends RoomDatabase {

    public abstract NodeDAO getNodeDao();

    public abstract NodeRelationDAO getNodeRelationDao();
}
