package com.myst3ry.storagetask.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.myst3ry.storagetask.data.model.Node;
import com.myst3ry.storagetask.data.model.NodeWithRelations;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface NodeDAO {

    @Transaction
    @Query("SELECT * FROM node")
    Flowable<List<NodeWithRelations>> getAllNodes();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNode(final Node node);
}
