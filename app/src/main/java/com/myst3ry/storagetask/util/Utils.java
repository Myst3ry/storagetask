package com.myst3ry.storagetask.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.myst3ry.storagetask.R;
import com.myst3ry.storagetask.ui.NodeType;

public final class Utils {

    private Utils() { }

    public static int getItemColor(final Context context, final int type) {
        if (type == NodeType.NODE_WITH_CHILDREN_AND_PARENTS.getType()) {
            return ContextCompat.getColor(context, R.color.colorRed);
        } else if (type == NodeType.NODE_WITH_PARENTS.getType()) {
            return ContextCompat.getColor(context, R.color.colorBlue);
        } else if (type == NodeType.NODE_WITH_CHILDREN.getType()) {
            return ContextCompat.getColor(context, R.color.colorYellow);
        } else {
            return ContextCompat.getColor(context, R.color.colorTransparent);
        }
    }

    public static int getRelationItemColor(final Context context, final int type) {
        if (type == NodeType.NODE_DETAILS_WITH_RELATIONS.getType()) {
            return ContextCompat.getColor(context, R.color.colorGreen);
        } else {
            return ContextCompat.getColor(context, R.color.colorTransparent);
        }
    }
}
