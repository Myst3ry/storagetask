package com.myst3ry.storagetask.ui.node_details;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.myst3ry.storagetask.ui.RelationType;

public final class NodeRelationsPagerAdapter extends FragmentStatePagerAdapter {

    private final String[] pageSections;
    private final long currentNodeId;

    public NodeRelationsPagerAdapter(final FragmentManager fragmentManager, final String[] sections,
                                     final long currentNodeId) {
        super(fragmentManager);
        this.pageSections = sections;
        this.currentNodeId = currentNodeId;
    }

    @Override
    public Fragment getItem(final int position) {
        return RelationsFragment.newInstance(RelationType.values()[position], currentNodeId);
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return pageSections[position];
    }

    @Override
    public int getCount() {
        return pageSections.length;
    }


}