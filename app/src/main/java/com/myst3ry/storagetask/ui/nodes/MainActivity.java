package com.myst3ry.storagetask.ui.nodes;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.myst3ry.storagetask.R;
import com.myst3ry.storagetask.StorageTaskApp;
import com.myst3ry.storagetask.data.model.Node;
import com.myst3ry.storagetask.ui.node_details.NodeDetailsActivity;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public final class MainActivity extends AppCompatActivity implements CreateNodeDialogFragment.OnNodeCreateListener {

    private final CompositeDisposable disposables = new CompositeDisposable();
    private NodesAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();

        initAdapter();
        initRecyclerView();
        initFab();
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestNodesList();
    }

    private void setupToolbar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initAdapter() {
        adapter = new NodesAdapter(nodeId -> startActivity(NodeDetailsActivity
                .newIntent(MainActivity.this, nodeId)));
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.rv_nodes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void initFab() {
        final FloatingActionButton fab = findViewById(R.id.fab_add_node);
        fab.setOnClickListener(view -> {
            final CreateNodeDialogFragment dialogFragment = CreateNodeDialogFragment.newInstance();
            dialogFragment.setCancelable(false);
            dialogFragment.show(getSupportFragmentManager(), null);
        });
    }

    private void requestNodesList() {
        disposables.add(StorageTaskApp.getApp(this).getDb().getNodeDao().getAllNodes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nodes -> adapter.setData(nodes),
                        t -> Log.w(getClass().getSimpleName(), t.getLocalizedMessage()))
        );
    }

    @Override
    public void onNodeCreate(final int value) {
        Completable.fromRunnable(() -> StorageTaskApp.getApp(MainActivity.this)
                .getDb()
                .getNodeDao()
                .insertNode(new Node(value))
        ).subscribeOn(Schedulers.newThread())
                .doAfterTerminate(() -> recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1))
                .subscribe();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isDestroyed() && !disposables.isDisposed()) {
            disposables.dispose();
        }
    }
}
