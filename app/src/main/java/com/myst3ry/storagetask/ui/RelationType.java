package com.myst3ry.storagetask.ui;

public enum RelationType {
    PARENT, CHILD
}
