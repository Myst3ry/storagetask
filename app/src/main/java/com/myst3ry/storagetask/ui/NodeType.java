package com.myst3ry.storagetask.ui;

public enum NodeType {
    NODE_WITHOUT_RELATIONS(0),
    NODE_WITH_CHILDREN(1),
    NODE_WITH_PARENTS(2),
    NODE_WITH_CHILDREN_AND_PARENTS(3),
    NODE_DETAILS_WITHOUT_RELATIONS(4),
    NODE_DETAILS_WITH_RELATIONS(5);

    private final int type;

    NodeType(final int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
