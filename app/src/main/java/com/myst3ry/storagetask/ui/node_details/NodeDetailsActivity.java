package com.myst3ry.storagetask.ui.node_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.myst3ry.storagetask.R;

public final class NodeDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_NODE_ID = "NODE_ID";

    private long nodeId;

    public static Intent newIntent(final Context context, final long nodeId) {
        final Intent intent = new Intent(context, NodeDetailsActivity.class);
        intent.putExtra(EXTRA_NODE_ID, nodeId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_details);
        setupToolbar();

        nodeId = getIntent().getLongExtra(EXTRA_NODE_ID, -1);
        initViewPager();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupToolbar() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViewPager() {
        final ViewPager pager = findViewById(R.id.view_pager);
        final TabLayout tabLayout = findViewById(R.id.tab_layout);

        pager.setAdapter(new NodeRelationsPagerAdapter(getSupportFragmentManager(),
                getResources().getStringArray(R.array.join_sections), nodeId));
        tabLayout.setupWithViewPager(pager);
    }
}
