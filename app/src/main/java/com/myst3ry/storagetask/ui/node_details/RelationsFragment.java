package com.myst3ry.storagetask.ui.node_details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myst3ry.storagetask.R;
import com.myst3ry.storagetask.StorageTaskApp;
import com.myst3ry.storagetask.data.model.NodeRelation;
import com.myst3ry.storagetask.data.model.NodeWithRelations;
import com.myst3ry.storagetask.ui.RelationType;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public final class RelationsFragment extends Fragment {

    private static final String ARG_CURRENT_ID = "NODE_ID";
    private static final String ARG_TYPE = "RELATION_TYPE";

    private RelationType type;
    private long currentNodeId;

    private CompositeDisposable disposables = new CompositeDisposable();
    private NodeWithRelations currentNode;
    private NodeRelationsAdapter adapter;

    public static RelationsFragment newInstance(final RelationType type, final long currentNodeId) {
        final RelationsFragment fragment = new RelationsFragment();
        final Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        args.putLong(ARG_CURRENT_ID, currentNodeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = (RelationType) getArguments().getSerializable(ARG_TYPE);
            currentNodeId = getArguments().getLong(ARG_CURRENT_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_relations, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initAdapter();
        initRecyclerView(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        requestNodeRelations();
    }

    private void initAdapter() {
        adapter = new NodeRelationsAdapter();
    }

    private void initRecyclerView(final View view) {
        final RecyclerView recyclerView = view.findViewById(R.id.rv_node_relations);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }

    private void requestNodeRelations() {
        disposables.add(StorageTaskApp.getApp(getActivity())
                .getDb().getNodeDao().getAllNodes()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map(this::prepareNodeRelations)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setNodeRelations,
                        t -> Log.w(getClass().getSimpleName(), t.getLocalizedMessage()))
        );
    }

    private List<NodeWithRelations> prepareNodeRelations(final List<NodeWithRelations> nodes) {
        final List<NodeWithRelations> nodesList = new ArrayList<>();
        for (NodeWithRelations node : nodes) {
            if (currentNodeId == node.getNode().getId()) {
                currentNode = node;
                break;
            }
        }

        if (type == RelationType.CHILD) {
            nodesList.addAll(currentNode.getChildrenFrom(nodes));
        } else if (type == RelationType.PARENT) {
            nodesList.addAll(currentNode.getParentsFrom(nodes));
        }

        for (NodeWithRelations node : nodes) {
            final long nodeId = node.getNode().getId();
            if (!currentNode.isChildOf(nodeId) && !currentNode.isParentOf(nodeId)
                    && currentNodeId != nodeId) {
                nodesList.add(node);
            }
        }

        return nodesList;
    }

    private void setNodeRelations(final List<NodeWithRelations> nodes) {
        adapter.setCurrentNode(currentNode);
        adapter.setListener(this::changeRelation);
        adapter.setData(nodes);
    }

    private void changeRelation(final long selectedNodeId) {
        if (type == RelationType.CHILD) {
            if (currentNode.isParentOf(selectedNodeId)) {
                deleteRelation(currentNodeId, selectedNodeId);
            } else {
                addRelation(currentNodeId, selectedNodeId);
            }
        } else if (type == RelationType.PARENT) {
            if (currentNode.isChildOf(selectedNodeId)) {
                deleteRelation(selectedNodeId, currentNodeId);
            } else {
                addRelation(selectedNodeId, currentNodeId);
            }
        }
    }

    private void addRelation(final long parentId, final long childId) {
        Completable.fromRunnable(() -> StorageTaskApp.getApp(getActivity())
                .getDb()
                .getNodeRelationDao()
                .insertNodeRelation(new NodeRelation(parentId, childId))
        ).subscribeOn(Schedulers.newThread())
                .subscribe();
    }

    private void deleteRelation(final long parentId, final long childId) {
        Completable.fromRunnable(() -> StorageTaskApp.getApp(getActivity())
                .getDb()
                .getNodeRelationDao()
                .deleteNodeRelation(new NodeRelation(parentId, childId))
        ).subscribeOn(Schedulers.newThread())
                .subscribe();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }
}
