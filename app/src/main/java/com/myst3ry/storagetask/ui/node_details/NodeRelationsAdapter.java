package com.myst3ry.storagetask.ui.node_details;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.storagetask.R;
import com.myst3ry.storagetask.data.model.NodeWithRelations;
import com.myst3ry.storagetask.ui.NodeType;
import com.myst3ry.storagetask.util.Utils;

import java.util.ArrayList;
import java.util.List;

public final class NodeRelationsAdapter extends RecyclerView.Adapter<NodeRelationsAdapter.NodeRelationViewHolder> {

    private List<NodeWithRelations> nodes;
    private OnRelationItemClickListener listener;
    private NodeWithRelations currentNode;

    public NodeRelationsAdapter() {
        this.nodes = new ArrayList<>();
//        this.listener = listener;
//        this.currentNode = currentNode;
    }

    @NonNull
    @Override
    public NodeRelationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_node, parent, false);
        layout.setBackgroundColor(Utils.getRelationItemColor(parent.getContext(), viewType));
        return new NodeRelationViewHolder(layout, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull NodeRelationViewHolder holder, int position) {
        holder.bind(nodes.get(position));
    }

    @Override
    public int getItemCount() {
        return nodes.size();
    }

    @Override
    public int getItemViewType(int position) {
        final NodeWithRelations node = nodes.get(position);

        if (currentNode.isChildOf(node.getNode().getId()) || currentNode.isParentOf(node.getNode().getId())) {
            return NodeType.NODE_DETAILS_WITH_RELATIONS.getType();
        } else {
            return NodeType.NODE_DETAILS_WITHOUT_RELATIONS.getType();
        }
    }

    public void setData(final List<NodeWithRelations> nodes) {
        this.nodes = nodes;
        notifyDataSetChanged();
    }

    public void setListener(final OnRelationItemClickListener listener) {
        this.listener = listener;
    }

    public void setCurrentNode(final NodeWithRelations currentNode) {
        this.currentNode = currentNode;
    }

    final static class NodeRelationViewHolder extends RecyclerView.ViewHolder {

        final OnRelationItemClickListener listener;
        final TextView descriptionTextView;

        NodeRelationViewHolder(@NonNull final View itemView, final OnRelationItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            descriptionTextView = itemView.findViewById(R.id.tv_node_desc);
        }

        void bind(final NodeWithRelations node) {
            itemView.setOnClickListener(view -> listener.onClick(node.getNode().getId()));
            descriptionTextView.setText(itemView.getContext().getString(R.string.node_description,
                    node.getNode().getValue()));
        }
    }

    public interface OnRelationItemClickListener {
        void onClick(final long nodeId);
    }
}
