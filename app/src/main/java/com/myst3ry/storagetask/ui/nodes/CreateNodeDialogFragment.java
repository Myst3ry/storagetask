package com.myst3ry.storagetask.ui.nodes;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.Toast;

import com.myst3ry.storagetask.R;

public final class CreateNodeDialogFragment extends DialogFragment {

    private OnNodeCreateListener listener;

    public static CreateNodeDialogFragment newInstance() {
        final CreateNodeDialogFragment dialogFragment = new CreateNodeDialogFragment();
        final Bundle args = new Bundle();
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNodeCreateListener) {
            listener = (OnNodeCreateListener) context;
        } else {
            throw new ClassCastException("Activity must implement OnNodeCreateListener interface");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.create_node_title)
                .setView(R.layout.dialog_create_node)
                .setPositiveButton(R.string.btn_create, (dialog, which) -> {
                    final TextInputEditText valueEditText = getDialog().findViewById(R.id.et_node_value);
                    if (valueEditText != null && valueEditText.getText() != null) {
                        final String value = valueEditText.getText().toString();
                        if (!TextUtils.isEmpty(value)) {
                            listener.onNodeCreate(Integer.valueOf(value));
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.err_empty_value), Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.btn_cancel, (dialog, which) -> dialog.dismiss())
                .create();
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    public interface OnNodeCreateListener {
        void onNodeCreate(final int value);
    }
}

