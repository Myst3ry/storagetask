package com.myst3ry.storagetask.ui.nodes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.myst3ry.storagetask.R;
import com.myst3ry.storagetask.data.model.NodeWithRelations;
import com.myst3ry.storagetask.ui.NodeType;
import com.myst3ry.storagetask.util.Utils;

import java.util.ArrayList;
import java.util.List;

public final class NodesAdapter extends RecyclerView.Adapter<NodesAdapter.NodeViewHolder> {

    private List<NodeWithRelations> nodes;
    private final OnItemClickListener listener;

    public NodesAdapter(final OnItemClickListener listener) {
        this.nodes = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public NodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_node, parent, false);
        layout.setBackgroundColor(Utils.getItemColor(parent.getContext(), viewType));
        return new NodeViewHolder(layout, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull NodeViewHolder holder, int position) {
        holder.bind(nodes.get(position));
    }

    @Override
    public int getItemCount() {
        return nodes.size();
    }

    @Override
    public int getItemViewType(int position) {
        final NodeWithRelations node = nodes.get(position);

        if (node.getParents().isEmpty() && node.getChildren().isEmpty()) {
            return NodeType.NODE_WITHOUT_RELATIONS.getType();
        } else if (node.getChildren().isEmpty()) {
            return NodeType.NODE_WITH_PARENTS.getType();
        } else if (node.getParents().isEmpty()) {
            return NodeType.NODE_WITH_CHILDREN.getType();
        } else {
            return NodeType.NODE_WITH_CHILDREN_AND_PARENTS.getType();
        }
    }

    public void setData(final List<NodeWithRelations> nodes) {
        this.nodes = nodes;
        notifyDataSetChanged();
    }

    final static class NodeViewHolder extends RecyclerView.ViewHolder {

        final OnItemClickListener listener;
        final TextView descriptionTextView;

        NodeViewHolder(@NonNull final View itemView, final OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            descriptionTextView = itemView.findViewById(R.id.tv_node_desc);
        }

        void bind(final NodeWithRelations node) {
            itemView.setOnClickListener(view -> listener.onClick(node.getNode().getId()));
            descriptionTextView.setText(itemView.getContext().getString(R.string.node_description,
                    node.getNode().getValue()));
        }
    }

    public interface OnItemClickListener {
        void onClick(final long nodeId);
    }
}
